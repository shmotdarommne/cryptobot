package ru.crypto.cryptobot.redis;


import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.stereotype.Component;

@Component
public class RedisPublisherImpl implements IRedisPublisher {
    private final RedisTemplate<String, Object> template;
    private final ChannelTopic topicTriggers;


    public RedisPublisherImpl(final RedisTemplate<String, Object> template,
                              final ChannelTopic topicTriggers) {
        this.template = template;
        this.topicTriggers = topicTriggers;
    }

    public void publish(String message) {
        template.convertAndSend(topicTriggers.getTopic(), message);
    }
}
