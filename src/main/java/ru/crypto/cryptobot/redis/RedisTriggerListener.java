package ru.crypto.cryptobot.redis;

import lombok.SneakyThrows;
import org.json.JSONObject;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import ru.crypto.cryptobot.singletons.CryptoSingleton;

public class RedisTriggerListener implements MessageListener {
    @SneakyThrows
    @Override
    public void onMessage(final Message message, final byte[] pattern) {


        CryptoSingleton cryptoSingleton = CryptoSingleton.getInstance();
        JSONObject json = new JSONObject(message.toString());

        String key = json.keys().next();
        cryptoSingleton.getMap().put(key, json.get(key));

    }
}
