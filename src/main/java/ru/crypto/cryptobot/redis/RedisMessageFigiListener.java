package ru.crypto.cryptobot.redis;

import lombok.SneakyThrows;
import org.json.JSONObject;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import ru.crypto.cryptobot.services.MapperToObject;

import java.util.Map;

public class RedisMessageFigiListener implements MessageListener {
    @SneakyThrows
    @Override
    public void onMessage(final Message message, final byte[] pattern) {

        JSONObject json = new JSONObject(message.toString());

        Map<String, Object> map = MapperToObject.toMap(json);

    }
}
