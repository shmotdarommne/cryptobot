package ru.crypto.cryptobot.singletons;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Setter
@Getter
public class CryptoSingleton {
    private static CryptoSingleton instance;

    private Map<String, Object> map = new HashMap<>();

    private CryptoSingleton() {
        map.put("BTC", 0.002424);
        map.put("ETH", 0.2412351235);
        map.put("BNB", 0.00452624502424);
        map.put("DOGE", 0.34573457345);
        map.put("DOT", 0.23523526);
        map.put("ADA", 0.34634634);
    }

    public static CryptoSingleton getInstance() {
        if (instance == null) {
            instance = new CryptoSingleton();
        }
        return instance;
    }
}
