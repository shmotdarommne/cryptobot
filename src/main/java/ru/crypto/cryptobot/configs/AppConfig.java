package ru.crypto.cryptobot.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.crypto.cryptobot.redis.IRedisPublisher;
import ru.crypto.cryptobot.redis.RedisMessageFigiListener;
import ru.crypto.cryptobot.redis.RedisPublisherImpl;
import ru.crypto.cryptobot.redis.RedisTriggerListener;


@Configuration
@EnableScheduling
public class AppConfig {
    @Bean
    JedisConnectionFactory jedisConnectionFactory() {
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setPassword("sOmE_sEcUrE_pAsS");
        return jedisConnectionFactory;
    }

    @Bean
    RedisTemplate<String, Object> redisTemplate() {
        final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();

        template.setConnectionFactory(jedisConnectionFactory());
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericToStringSerializer<Object>(Object.class));
        template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
        return template;
    }


    @Bean
    MessageListenerAdapter messageListenerFigi() {
        return new MessageListenerAdapter(new RedisMessageFigiListener());
    }

    @Bean
    MessageListenerAdapter messageListenerTrigger() {
        return new MessageListenerAdapter(new RedisTriggerListener());
    }

    @Bean
    RedisMessageListenerContainer redisContainer() {
        final RedisMessageListenerContainer container = new RedisMessageListenerContainer();


        container.setConnectionFactory(jedisConnectionFactory());
        container.addMessageListener(messageListenerFigi(), topicFIGI());
        container.addMessageListener(messageListenerTrigger(), topicEvents());

        return container;
    }

    @Bean
    IRedisPublisher redisPublisher() {
        return new RedisPublisherImpl(redisTemplate(), topicTriggers());
    }

    @Bean
    ChannelTopic topicTriggers() {
        return new ChannelTopic("stream_triggers");
    }

    ChannelTopic topicFIGI() {
        return new ChannelTopic("stream_currency");
    }

    ChannelTopic topicEvents() {
        return new ChannelTopic("events_stream");
    }
}
