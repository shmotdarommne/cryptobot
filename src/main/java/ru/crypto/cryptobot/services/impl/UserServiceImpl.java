package ru.crypto.cryptobot.services.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.crypto.cryptobot.models.entities.Cryptocurrency;
import ru.crypto.cryptobot.models.entities.User;
import ru.crypto.cryptobot.repositories.CryptocurrencyRepository;
import ru.crypto.cryptobot.repositories.UserRepository;
import ru.crypto.cryptobot.services.UserService;

import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final CryptocurrencyRepository cryptocurrencyRepository;

    @Override
    public User saveAll(Set<Cryptocurrency> cryptocurrencySet) {
        return saveAll(cryptocurrencySet);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public User addAllSub(Integer userId) {
        User user = userRepository.getById(userId);
        user.getCryptocurrencies().addAll(cryptocurrencyRepository.findAll());
        userRepository.save(user);
        return user;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void deleteAllSub(Integer userId) {
        User user = userRepository.getById(userId);
        user.getCryptocurrencies().clear();
        userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public User addSubToUser(Integer userId, Cryptocurrency cryptocurrency) {
        User user = userRepository.getById(userId);
        user.getCryptocurrencies().add(cryptocurrency);
        return userRepository.save(user);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void deleteSub(Integer userId, Cryptocurrency cryptocurrency) {
        User user = userRepository.getById(userId);
        user.getCryptocurrencies().remove(cryptocurrency);
        userRepository.save(user);
    }

    @Transactional(readOnly = true)
    @Override
    public User findById(Integer userId) {
        return userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }
}
