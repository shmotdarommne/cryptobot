package ru.crypto.cryptobot.services.impl;

import org.springframework.stereotype.Service;
import ru.crypto.cryptobot.models.entities.Cryptocurrency;
import ru.crypto.cryptobot.repositories.CryptocurrencyRepository;
import ru.crypto.cryptobot.services.CryptocurrencyService;

import java.util.List;

@Service
public class CryptocurrencyServiceImpl implements CryptocurrencyService {

    private final CryptocurrencyRepository cryptocurrencyRepository;

    public CryptocurrencyServiceImpl(CryptocurrencyRepository cryptocurrencyRepository) {
        this.cryptocurrencyRepository = cryptocurrencyRepository;
    }

    @Override
    public List<Cryptocurrency> getAll() {
        return cryptocurrencyRepository.findAll();
    }

    @Override
    public Cryptocurrency save(Cryptocurrency cryptocurrency) {
        return cryptocurrencyRepository.save(cryptocurrency);
    }

    @Override
    public Cryptocurrency getByName(String name) {
        return cryptocurrencyRepository.findByName(name);
    }
}
