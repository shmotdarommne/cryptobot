package ru.crypto.cryptobot.services;

import ru.crypto.cryptobot.models.entities.Cryptocurrency;
import ru.crypto.cryptobot.models.entities.User;

import java.util.List;
import java.util.Set;

public interface UserService {
    User findById(Integer userId);

    User saveUser(User user);

    User addSubToUser(Integer userId, Cryptocurrency cryptocurrency);

    void deleteSub(Integer userId, Cryptocurrency cryptocurrency);

    User addAllSub(Integer userId);

    void deleteAllSub(Integer userId);

    List<User> getAll();

    User saveAll(Set<Cryptocurrency> cryptocurrencySet);
}
