package ru.crypto.cryptobot.services;

import ru.crypto.cryptobot.models.entities.Cryptocurrency;

import java.util.List;

public interface CryptocurrencyService {
    Cryptocurrency getByName(String Name);

    List<Cryptocurrency> getAll();

    Cryptocurrency save(Cryptocurrency cryptocurrency);
}
