package ru.crypto.cryptobot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.crypto.cryptobot.models.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
