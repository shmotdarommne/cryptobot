package ru.crypto.cryptobot.botApi;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import ru.crypto.cryptobot.models.entities.Cryptocurrency;
import ru.crypto.cryptobot.models.entities.User;
import ru.crypto.cryptobot.redis.RedisPublisherImpl;
import ru.crypto.cryptobot.services.ReplyMessagesService;
import ru.crypto.cryptobot.services.UserService;
import ru.crypto.cryptobot.singletons.CryptoSingleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
@AllArgsConstructor
public class WriteMessage {

    private final ReplyMessagesService messagesService;
    private final UserService userService;
    private final RedisPublisherImpl redisPublisher;


    public SendMessage processUsersInput(BotState botState, Message inputMsg) {
        int userId = inputMsg.getFrom().getId();
        long chatId = inputMsg.getChatId();


        SendMessage replyToUser = null;

        if (botState.equals(BotState.SHOW_MAIN_MENU)) {

            replyToUser = messagesService.getReplyMessage(chatId, "Добро пожаловать! Меню в помощь юный падаван.");
        }

        if (botState.equals(BotState.SHOW_SUB_MENU)) {
            replyToUser = messagesService.getReplyMessage(chatId, "Выберите валюту");
            replyToUser.setReplyMarkup(getInlineMessageButtonsForSub());
            userService.saveUser(newUser(userId, BotState.SHOW_MAIN_MENU));
        }

        if (botState.equals(BotState.SHOW_UNSUB_MENU)) {
            replyToUser = messagesService.getReplyMessage(chatId, "Выберите валюту");
            replyToUser.setReplyMarkup(getInlineMessageButtonsForUnSub());
            userService.saveUser(newUser(userId, BotState.SHOW_MAIN_MENU));
        }

        if (botState.equals(BotState.SHOW_PRICE)) {
            CryptoSingleton cryptoSingleton = CryptoSingleton.getInstance();
            String str = String.format("PriceList: \n BTC = %f\n ETH = %f\n BNB = %f\n DOGE = %f\n DOT = %f\n ADA = %f\n",
                    cryptoSingleton.getMap().get("BTC"), cryptoSingleton.getMap().get("ETH"), cryptoSingleton.getMap().get("BNB"), cryptoSingleton.getMap().get("DOGE"),
                    cryptoSingleton.getMap().get("DOT"), cryptoSingleton.getMap().get("ADA"));
            replyToUser = messagesService.getReplyMessage(chatId, str);
            userService.saveUser(newUser(userId, BotState.SHOW_MAIN_MENU));
        }

        if (botState.equals(BotState.SHOW_SUBSCRIBERS)) {
            User user = userService.findById(userId);
            Set<Cryptocurrency> cryptocurrencies = user.getCryptocurrencies();
            StringBuilder stringBuilder = new StringBuilder();
            cryptocurrencies.stream().forEach(cryptocurrency -> stringBuilder.append(cryptocurrency.getName() + "\n"));

            replyToUser = messagesService.getReplyMessage(chatId, "Ваши подписки : \n" + stringBuilder.toString());
            userService.saveUser(newUser(userId, BotState.SHOW_MAIN_MENU));
        }

        if (botState.equals(BotState.SUBSCRIBE_TO_EVENT)) {
            replyToUser = messagesService.getReplyMessage(chatId, "К сожалению, пока у нас работает всего лишь одна подписка, а именно уведомление о повышении цены определенной валюты на какой то процент. Например : Валюта N изменилась на X% вверх(Пример ввода : BTC, 5, вверх)");
            userService.saveUser(newUser(userId, BotState.BOT_WAIT_ANSWER));
        }

        if (botState.equals(BotState.BOT_WAIT_ANSWER)) {
            String usersAnswer = inputMsg.getText();

            usersAnswer = usersAnswer.trim();
            usersAnswer = usersAnswer.replace(" ", "");

            String[] mass = usersAnswer.split(",");


            try {
                CryptoSingleton cryptoSingleton = CryptoSingleton.getInstance();
                Double percent = mass[1].equals("вниз") ? (1 - Integer.parseInt(mass[1]) * 0.01) : (1 + Integer.parseInt(mass[1]) * 0.01);
                Double target = (Double) cryptoSingleton.getMap().get(mass[0]) * percent;
                String dir = mass[1].equals("вниз") ? "0" : "1";
                String toScala = String.format("{ \"id\" : \"1\", \"FIGI\" : \"%s\", \"user_id\" : \"%s\", \"direction\" : \"%s\", \"target\" : \"%s\"}, ", mass[0].trim(), userId, dir, target);
                redisPublisher.publish(toScala);
            } catch (Exception e) {
                e.printStackTrace();
            }


            replyToUser = messagesService.getReplyMessage(chatId, "Вы подписались на событие");
            userService.saveUser(newUser(userId, BotState.SHOW_MAIN_MENU));
        }

        return replyToUser;
    }

    private User newUser(Integer userId, BotState botState) {
        User user = userService.findById(userId);
        return User.builder()
                .id(userId)
                .chatId(user.getChatId())
                .botState(botState)
                .cryptocurrencies(user.getCryptocurrencies())
                .build();
    }

    private InlineKeyboardMarkup getInlineMessageButtonsForSub() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton BTCButton = new InlineKeyboardButton().setText("BTC");
        InlineKeyboardButton ETHButton = new InlineKeyboardButton().setText("ETH");
        InlineKeyboardButton BNBButton = new InlineKeyboardButton().setText("BNB");
        InlineKeyboardButton DOGEButton = new InlineKeyboardButton().setText("DOGE");
        InlineKeyboardButton DOTButton = new InlineKeyboardButton().setText("DOT");
        InlineKeyboardButton ADAButton = new InlineKeyboardButton().setText("ADA");
        InlineKeyboardButton addALL = new InlineKeyboardButton().setText("Добавить все");

        BTCButton.setCallbackData("BTC_SUB");
        ETHButton.setCallbackData("ETH_SUB");
        BNBButton.setCallbackData("BNB_SUB");
        DOGEButton.setCallbackData("DOGE_SUB");
        DOTButton.setCallbackData("DOT_SUB");
        ADAButton.setCallbackData("ADA_SUB");
        addALL.setCallbackData("all_SUB");

        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
        keyboardButtonsRow1.add(BTCButton);
        keyboardButtonsRow1.add(ETHButton);
        keyboardButtonsRow1.add(BNBButton);

        List<InlineKeyboardButton> keyboardButtonsRow2 = new ArrayList<>();
        keyboardButtonsRow2.add(DOGEButton);
        keyboardButtonsRow2.add(DOTButton);
        keyboardButtonsRow2.add(ADAButton);

        List<InlineKeyboardButton> keyboardButtonsRow3 = new ArrayList<>();
        keyboardButtonsRow3.add(addALL);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);
        rowList.add(keyboardButtonsRow2);
        rowList.add(keyboardButtonsRow3);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
    }

    private InlineKeyboardMarkup getInlineMessageButtonsForUnSub() {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();

        InlineKeyboardButton BTCButton = new InlineKeyboardButton().setText("BTC");
        InlineKeyboardButton ETHButton = new InlineKeyboardButton().setText("ETH");
        InlineKeyboardButton BNBButton = new InlineKeyboardButton().setText("BNB");
        InlineKeyboardButton DOGEButton = new InlineKeyboardButton().setText("DOGE");
        InlineKeyboardButton DOTButton = new InlineKeyboardButton().setText("DOT");
        InlineKeyboardButton ADAButton = new InlineKeyboardButton().setText("ADA");
        InlineKeyboardButton addALL = new InlineKeyboardButton().setText("Отписаться от всех");

        BTCButton.setCallbackData("BTC_UNSUB");
        ETHButton.setCallbackData("ETH_UNSUB");
        BNBButton.setCallbackData("BNB_UNSUB");
        DOGEButton.setCallbackData("DOGE_UNSUB");
        DOTButton.setCallbackData("DOT_UNSUB");
        ADAButton.setCallbackData("ADA_UNSUB");
        addALL.setCallbackData("all_UNSUB");

        List<InlineKeyboardButton> keyboardButtonsRow1 = new ArrayList<>();
        keyboardButtonsRow1.add(BTCButton);
        keyboardButtonsRow1.add(ETHButton);
        keyboardButtonsRow1.add(BNBButton);

        List<InlineKeyboardButton> keyboardButtonsRow2 = new ArrayList<>();
        keyboardButtonsRow2.add(DOGEButton);
        keyboardButtonsRow2.add(DOTButton);
        keyboardButtonsRow2.add(ADAButton);

        List<InlineKeyboardButton> keyboardButtonsRow3 = new ArrayList<>();
        keyboardButtonsRow3.add(addALL);

        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();
        rowList.add(keyboardButtonsRow1);
        rowList.add(keyboardButtonsRow2);
        rowList.add(keyboardButtonsRow3);

        inlineKeyboardMarkup.setKeyboard(rowList);

        return inlineKeyboardMarkup;
    }
}

