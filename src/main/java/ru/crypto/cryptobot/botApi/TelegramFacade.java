package ru.crypto.cryptobot.botApi;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.crypto.cryptobot.models.entities.Cryptocurrency;
import ru.crypto.cryptobot.models.entities.User;
import ru.crypto.cryptobot.services.CryptocurrencyService;
import ru.crypto.cryptobot.services.MainMenuService;
import ru.crypto.cryptobot.services.UserService;

import java.util.List;
import java.util.Set;

@Component
@Slf4j
@AllArgsConstructor
public class TelegramFacade {
    private final WriteMessage writeMessage;
    private final UserService userService;
    private final MainMenuService mainMenuService;
    private final CryptocurrencyService cryptocurrencyService;


    public BotApiMethod handleUpdate(Update update) {
        SendMessage replyMessage = null;

        if (update.hasCallbackQuery()) {
            CallbackQuery callbackQuery = update.getCallbackQuery();
            log.info("New callbackQuery from User: {}, userId: {}, with data: {}", update.getCallbackQuery().getFrom().getUserName(),
                    callbackQuery.getFrom().getId(), update.getCallbackQuery().getData());
            return processCallbackQuery(callbackQuery);
        }

        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            log.info("New message from User:{}, chatId: {},  with text: {}",
                    message.getFrom().getUserName(), message.getChatId(), message.getText());
            replyMessage = handleInputMessage(message);
        }

        return replyMessage;
    }

    private Boolean hasCryptocurrency(Set<Cryptocurrency> cryptocurrencies, Cryptocurrency cryptocurrency) {
        return cryptocurrencies.stream().anyMatch(cryptocurrency1 -> cryptocurrency1.equals(cryptocurrency));
    }

    private BotApiMethod<?> processCallbackQuery(CallbackQuery buttonQuery) {
        final long chatId = buttonQuery.getMessage().getChatId();
        final int userId = buttonQuery.getFrom().getId();
        BotApiMethod<?> callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Воспользуйтесь главным меню");

        List<Cryptocurrency> list = cryptocurrencyService.getAll();

        if (list.isEmpty()) {
            cryptocurrencyService.save(Cryptocurrency.builder()
                    .name("BTC")
                    .price(2000.0242)
                    .build());
            cryptocurrencyService.save(Cryptocurrency.builder()
                    .name("ETH")
                    .price(2000.0242)
                    .build());
            cryptocurrencyService.save(Cryptocurrency.builder()
                    .name("BNB")
                    .price(2000.0242)
                    .build());
            cryptocurrencyService.save(Cryptocurrency.builder()
                    .name("DOGE")
                    .price(2000.0242)
                    .build());
            cryptocurrencyService.save(Cryptocurrency.builder()
                    .name("DOT")
                    .price(2000.0242)
                    .build());
            cryptocurrencyService.save(Cryptocurrency.builder()
                    .name("ADA")
                    .price(2000.0242)
                    .build());
        }

        User user = userService.findById(userId);
        if (user == null) {
            return callBackAnswer;
        }

        Set<Cryptocurrency> cryptocurrencies = user.getCryptocurrencies();

        if (buttonQuery.getData().equals("BTC_SUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("BTC");
            if (hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас уже есть подписка на BTC");
            } else {
                userService.addSubToUser(userId, cryptocurrencyService.getByName("BTC"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы подписались на BTC");
            }
        } else if (buttonQuery.getData().equals("ETH_SUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("ETH");
            if (hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас уже есть подписка на ETH");
            } else {
                userService.addSubToUser(userId, cryptocurrencyService.getByName("ETH"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы подписались на ETH");
            }
        } else if (buttonQuery.getData().equals("BNB_SUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("BNB");
            if (hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас уже есть подписка на BNB");
            } else {
                userService.addSubToUser(userId, cryptocurrencyService.getByName("BNB"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы подписались на BNB");
            }
        } else if (buttonQuery.getData().equals("DOGE_SUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("DOGE");
            if (hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас уже есть подписка на DOGE");
            } else {
                userService.addSubToUser(userId, cryptocurrencyService.getByName("DOGE"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы подписались на DOGE");
            }
        } else if (buttonQuery.getData().equals("DOT_SUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("DOT");
            if (hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас уже есть подписка на DOT");
            } else {
                userService.addSubToUser(userId, cryptocurrencyService.getByName("DOT"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы подписались на DOT");
            }
        } else if (buttonQuery.getData().equals("ADA_SUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("ADA");
            if (hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас уже есть подписка на ADA");
            } else {
                userService.addSubToUser(userId, cryptocurrencyService.getByName("ADA"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы подписались на ADA");
            }
        } else if (buttonQuery.getData().equals("all_SUB")) {
            userService.addAllSub(userId);
            callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Подписки оформлены");
        }

        cryptocurrencies = user.getCryptocurrencies();

        if (buttonQuery.getData().equals("BTC_UNSUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("BTC");
            if (!hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас нет подписки на BTC");
            } else {
                userService.deleteSub(userId, cryptocurrencyService.getByName("BTC"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы отписались от BTC");
            }
        } else if (buttonQuery.getData().equals("ETH_UNSUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("ETH");
            if (!hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас нет подписки на ETH");
            } else {
                userService.deleteSub(userId, cryptocurrencyService.getByName("ETH"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы отписались от ETH");
            }
        } else if (buttonQuery.getData().equals("BNB_UNSUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("BNB");
            if (!hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас нет подписки на BNB");
            } else {
                userService.deleteSub(userId, cryptocurrencyService.getByName("BNB"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы отписались от BNB");
            }
        } else if (buttonQuery.getData().equals("DOGE_UNSUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("DOGE");
            if (!hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас нет подписки на DOGE");
            } else {
                userService.deleteSub(userId, cryptocurrencyService.getByName("DOGE"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы отписались от DOGE");
            }
        } else if (buttonQuery.getData().equals("DOT_UNSUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("DOT");
            if (!hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас нет подписки на DOT");
            } else {
                userService.deleteSub(userId, cryptocurrencyService.getByName("DOT"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы отписались от DOT");
            }
        } else if (buttonQuery.getData().equals("ADA_UNSUB")) {
            Cryptocurrency cryptocurrency = cryptocurrencyService.getByName("ADA");
            if (!hasCryptocurrency(cryptocurrencies, cryptocurrency)) {
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "У вас нет подписки на ADA");
            } else {
                userService.deleteSub(userId, cryptocurrencyService.getByName("ADA"));
                callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Вы отписались от ADA");
            }
        } else if (buttonQuery.getData().equals("all_UNSUB")) {
            userService.deleteAllSub(userId);
            callBackAnswer = mainMenuService.getMainMenuMessage(chatId, "Подписки удалены");
        }

        return callBackAnswer;
    }

    private SendMessage handleInputMessage(Message message) {
        String inputMsg = message.getText();
        Integer userId = message.getFrom().getId();
        BotState botState = BotState.SHOW_MAIN_MENU;
        SendMessage replyMessage;


        switch (inputMsg) {
            case "/start":
                botState = BotState.SHOW_MAIN_MENU;
                break;
            case "Подписаться":
                botState = BotState.SHOW_SUB_MENU;
                break;
            case "Отписаться":
                botState = BotState.SHOW_UNSUB_MENU;
                break;
            case "Мои подписки":
                botState = BotState.SHOW_SUBSCRIBERS;
                break;
            case "Узнать курс криптовалют":
                botState = BotState.SHOW_PRICE;
                break;
            case "Подписаться на событие":
                botState = BotState.SUBSCRIBE_TO_EVENT;
                break;
            default:
                try {
                    botState = userService.findById(userId).getBotState();
                    if (botState == null) {
                        botState = BotState.SHOW_MAIN_MENU;
                    }
                } catch (Exception e) {
                    userService.saveUser(User.builder()
                            .chatId(message.getChatId())
                            .id(userId)
                            .build());
                }
                break;
        }
        try {
            User user = userService.findById(userId);
            userService.saveUser(User.builder()
                    .id(userId)
                    .chatId(message.getChatId())
                    .cryptocurrencies(user.getCryptocurrencies())
                    .botState(botState)
                    .build());
        } catch (Exception e) {
            userService.saveUser(User.builder()
                    .id(userId)
                    .chatId(message.getChatId())
                    .botState(botState)
                    .build());
        }


        replyMessage = writeMessage.processUsersInput(botState, message);
        if (botState.equals(BotState.SHOW_MAIN_MENU)) {
            replyMessage.setReplyMarkup(mainMenuService.getMainMenuKeyboard());
        }

        return replyMessage;
    }

}
