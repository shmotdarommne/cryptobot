package ru.crypto.cryptobot.botApi;

public enum BotState {
    SHOW_MAIN_MENU,
    SHOW_SUB_MENU,
    SHOW_UNSUB_MENU,
    SHOW_SUBSCRIBERS,
    SHOW_PRICE,
    SUBSCRIBE_TO_EVENT,
    BOT_WAIT_ANSWER
}
