package ru.crypto.cryptobot.models.entities;

import lombok.*;
import ru.crypto.cryptobot.botApi.BotState;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    private Integer id;

    private Long chatId;

    private BotState botState;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "user_cryptocurrency",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "cryptocurrency_id"))
    private Set<Cryptocurrency> cryptocurrencies;

}
